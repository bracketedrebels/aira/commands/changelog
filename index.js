'use strict';

const tags = require('bluebird').promisify(require('semver-tags'));
const commits = require('commits-between');
const parser = require('git-commit-parser');
const debug = require('debug')(require('./package.json').name)
const fs = require('fs');
const mustache = require('mustache');

const defaultTemplate = `{{#commits}}• {{toString}}\n{{/commits}}`;

exports.command = 'changelog'
exports.desc = 'Build changelog based on git commits history.'
exports.builder = (yargs) => yargs
  .option('r', {
    description: 'Tags range descriptor. Descriptor mimics standard git revision range descriptor, but only for tags. If no range specified then last two tags will be used as range.',
    type: 'string',
    alias: 'range'
  })
  .option('t', {
    description: 'Provide path to template that will be used for changelog rendering. If omitted, default unordered list style will be used.',
    type: 'string',
    alias: 'template'
  })
  .check(({r, t}) => {
    if (r && !(/.+?\.\..+/.test(r))) throw new Error(`Invalid formatted range '${r}'`);
    if (t && !fs.existsSync(t)) throw new Error(`Provided template file seem does not exist`);
    return true;
  });

exports.handler = ({ range, template }) => {
  (range
    ? (debug('Range:', range), commits(range
      .split('..', 2)
      .reduce((acc, v, i) => ({...acc, [['from', 'to'][i]]: v}), {})))
    : tags({ last: 2 })
      .then(([from, to = 'HEAD']) => (debug('Range:', `${from}..${to}`), commits({ from, to }))))
    .then(cmts => cmts
      .map(({ subject }) => {
        try {
          return parser.parse(subject).header;
        } catch (e) {
          return null
        }
      })
      .filter(v => !!v))
    .then(v => (debug('Commits:\n', v), v))
    .then(commits => mustache.render(template && fs.readFileSync(template).toString() || defaultTemplate, {commits}))
    .then(v => (debug('Changelog:\n', v), process.stdout.write(v || 'Changelog is empty')))
    .catch(err => (console.error(err), process.exit(1)));
}
