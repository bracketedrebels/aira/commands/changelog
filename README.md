Changelog - AIRA module
=======================

Generate changelog for your release, based on commits between tags.

Usage
-----

```shell
npm install -g @bracketedrebels/aira @braira/changelog
aira changelog --help
```

Example
-------
Send Discord notification via the webhook together with changelog, based on commits between last two tags.

.notification.discord.json
```json
{
  "embeds": [
    {
      "title": "My Awesome app released!",
      "description": "{{{chlog}}}"
    }
  ]
}
```

.changelog.mustache
```mustache
Changelog:

{{#commits}}
• {{type}}: {{subject}}
{{/commits}}
```

CMD
```shell
npm install -g @bracketedrebels/aira @braira/changelog @braira/notify
export CHLOG=$(aira changelog --template=.changelog.mustache)
aira notify <webhook> .notification.discord.json --payload.chlog=$CHLOG
```
